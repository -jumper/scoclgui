scoclGUI is a .NET front end for Alexander Blade's GTA IV sco compiler, scocl.

Requirements:

- Win XP or newer, with .NET framework 2.0 or higher
- scocl that has been updated by Three-Socks (included)


Details:

- batch compiles multiple sources for multiple platforms, with all scocl options available
- settings are automatically stored via .ini, in the same directory as the program
- accepts sources passed as arguments, sources dragged directly to the running program, or sources added via traditional file menu
- accepts a folder of sources via drag n drop or arguments (non-recursive, folders inside the folder will not be added)
- filters out duplicate input files, as well as any file that is not .c or .cpp
- filters out files containing "#pragma once" (optional - sources are only scanned if this is enabled)
- compression support is experimental and a work in progress!


For any info on additional scocl options, you can refer to the documentation included with scocl.