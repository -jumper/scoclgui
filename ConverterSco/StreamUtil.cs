﻿/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
 * ConverterSco by Cathering
 * modified by jumper
 * 
 * usage:
 * D:\ConverterSco.exe the_script_you_want_to_change.sco
 * 
 * Multiple files are not supported at this time
 * -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

using System;
using System.IO;

namespace ConverterSco
{
	public static class StreamUtil
	{
		private const int DefaultBufferSize = 8192;

		public static byte[] ReadFully(Stream input)
		{
			return StreamUtil.ReadFully(input, 8192);
		}

		public static byte[] ReadFully(Stream input, int bufferSize)
		{
			if (bufferSize < 1)
			throw new ArgumentOutOfRangeException("bufferSize");
			else
			return StreamUtil.ReadFully(input, new byte[bufferSize]);
		}

		public static byte[] ReadFully(Stream input, IBuffer buffer)
		{
			if (buffer == null)
			throw new ArgumentNullException("buffer");
			else
			return StreamUtil.ReadFully(input, buffer.Bytes);
		}

		public static byte[] ReadFully(Stream input, byte[] buffer)
		{
			if (buffer == null)
			throw new ArgumentNullException("buffer");
			if (input == null)
			throw new ArgumentNullException("input");
			if (buffer.Length == 0)
			throw new ArgumentException("Buffer has length of 0");
			using (MemoryStream memoryStream = new MemoryStream())
			{
				StreamUtil.Copy(input, (Stream) memoryStream, buffer);
				if (memoryStream.Length == (long) memoryStream.GetBuffer().Length)
				return memoryStream.GetBuffer();
				else
				return memoryStream.ToArray();
			}
		}

		public static void Copy(Stream input, Stream output)
		{
			StreamUtil.Copy(input, output, 8192);
		}

		public static void Copy(Stream input, Stream output, int bufferSize)
		{
			if (bufferSize < 1)
			throw new ArgumentOutOfRangeException("bufferSize");
			StreamUtil.Copy(input, output, new byte[bufferSize]);
		}

		public static void Copy(Stream input, Stream output, IBuffer buffer)
		{
			if (buffer == null)
			throw new ArgumentNullException("buffer");
			StreamUtil.Copy(input, output, buffer.Bytes);
		}

		public static void Copy(Stream input, Stream output, byte[] buffer)
		{
			if (buffer == null)
			throw new ArgumentNullException("buffer");
			if (input == null)
			throw new ArgumentNullException("input");
			if (output == null)
			throw new ArgumentNullException("output");
			if (buffer.Length == 0)
			throw new ArgumentException("Buffer has length of 0");
			try
			{
				input.Position = 0L;
				output.Position = 0L;
			}
			catch (NotSupportedException ex)
			{
			}
			int count;
			while ((count = input.Read(buffer, 0, buffer.Length)) > 0)
			output.Write(buffer, 0, count);
		}

		public static byte[] ReadExactly(Stream input, int bytesToRead)
		{
			return StreamUtil.ReadExactly(input, new byte[bytesToRead]);
		}

		public static byte[] ReadExactly(Stream input, IBuffer buffer)
		{
			return StreamUtil.ReadExactly(input, buffer.Bytes);
		}

		public static byte[] ReadExactly(Stream input, byte[] buffer)
		{
			return StreamUtil.ReadExactly(input, buffer, buffer.Length);
		}

		public static byte[] ReadExactly(Stream input, IBuffer buffer, int bytesToRead)
		{
			return StreamUtil.ReadExactly(input, buffer.Bytes, bytesToRead);
		}

		public static byte[] ReadExactly(Stream input, byte[] buffer, int bytesToRead)
		{
			return StreamUtil.ReadExactly(input, buffer, 0, bytesToRead);
		}

		public static byte[] ReadExactly(Stream input, IBuffer buffer, int startIndex, int bytesToRead)
		{
			return StreamUtil.ReadExactly(input, buffer.Bytes, 0, bytesToRead);
		}

		public static byte[] ReadExactly(Stream input, byte[] buffer, int startIndex, int bytesToRead)
		{
			if (input == null)
			throw new ArgumentNullException("input");
			if (buffer == null)
			throw new ArgumentNullException("buffer");
			if (startIndex < 0 || startIndex >= buffer.Length)
			throw new ArgumentOutOfRangeException("startIndex");
			if (bytesToRead < 1 || startIndex + bytesToRead > buffer.Length)
			throw new ArgumentOutOfRangeException("bytesToRead");
			int num1 = 0;
			while (num1 < bytesToRead)
			{
				int num2 = input.Read(buffer, startIndex + num1, bytesToRead - num1);
				if (num2 == 0)
				throw new EndOfStreamException(string.Format("End of stream reached with {0} byte{1} left to read.", (object) (bytesToRead - num1), bytesToRead - num1 == 1 ? (object) "s" : (object) ""));
				num1 += num2;
			}
			return buffer;
		}
	}
}
