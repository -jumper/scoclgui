﻿/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
 * ConverterSco by Cathering
 * modified by jumper
 * 
 * usage:
 * D:\ConverterSco.exe the_script_you_want_to_change.sco
 * 
 * Multiple files are not supported at this time
 * -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

using System.IO;
using zlib;

namespace ConverterSco
{
	internal static class Compression
	{
		private const int CopyBufferSize = 32768;

		public static void Compress(Stream source, Stream destination)
		{
			using (ZOutputStream zoutputStream = new ZOutputStream(destination, 9))
			StreamUtil.Copy(source, (Stream) zoutputStream, 32768);
		}

		public static void Decompress(Stream source, Stream destination)
		{
			using (ZOutputStream zoutputStream = new ZOutputStream(destination))
			StreamUtil.Copy(source, (Stream) zoutputStream, 32768);
		}
	}
}
