﻿/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
 * ConverterSco by Cathering
 * modified by jumper
 * 
 * usage:
 * D:\ConverterSco.exe the_script_you_want_to_change.sco
 * 
 * Multiple files are not supported at this time
 * -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

using System;
using System.Security.Cryptography;
using System.Text;

namespace ConverterSco
{
	internal static class Encryption
	{
		private static byte[] gtaIvKey = new byte[32]
		{
			(byte) 26,
			(byte) 181,
			(byte) 111,
			(byte) 237,
			(byte) 126,
			(byte) 195,
			byte.MaxValue,
			(byte) 1,
			(byte) 34,
			(byte) 123,
			(byte) 105,
			(byte) 21,
			(byte) 51,
			(byte) 151,
			(byte) 93,
			(byte) 206,
			(byte) 71,
			(byte) 215,
			(byte) 105,
			(byte) 101,
			(byte) 63,
			(byte) 247,
			(byte) 117,
			(byte) 66,
			(byte) 106,
			(byte) 150,
			(byte) 205,
			(byte) 109,
			(byte) 83,
			(byte) 7,
			(byte) 86,
			(byte) 93
		};

		static Encryption()
		{
		}

		public static byte[] Encrypt(byte[] dataIn, byte[] key)
		{
			if (dataIn == null)
			return (byte[]) null;
			Array.Resize<byte>(ref key, 32);
			int inputCount = dataIn.Length & -16;
			if (inputCount > 0)
			{
				ICryptoTransform encryptor = Encryption.GetEncryptor(key);
				for (int index = 0; index < 16; ++index)
				encryptor.TransformBlock(dataIn, 0, inputCount, dataIn, 0);
			}
			return dataIn;
		}

		public static byte[] Encrypt(byte[] dataIn)
		{
			return Encryption.Encrypt(dataIn, Encryption.gtaIvKey);
		}

		public static byte[] Decrypt(byte[] dataIn, byte[] key)
		{
			if (dataIn == null)
			return (byte[]) null;
			Array.Resize<byte>(ref key, 32);
			int inputCount = dataIn.Length & -16;
			if (inputCount > 0)
			{
				ICryptoTransform decryptor = Encryption.GetDecryptor(key);
				for (int index = 0; index < 16; ++index)
				decryptor.TransformBlock(dataIn, 0, inputCount, dataIn, 0);
			}
			return dataIn;
		}

		public static byte[] Decrypt(byte[] dataIn)
		{
			return Encryption.Decrypt(dataIn, Encryption.gtaIvKey);
		}

		public static uint OneAtATime(string name)
		{
			name = name.ToLower();
			byte[] bytes = Encoding.ASCII.GetBytes(name);
			uint num1 = 0U;
			foreach (byte num2 in bytes)
			{
				uint num3 = num1 + (uint) num2;
				uint num4 = num3 + (num3 << 10);
				num1 = num4 ^ num4 >> 6;
			}
			uint num5 = num1 + (num1 << 3);
			uint num6 = num5 ^ num5 >> 11;
			return num6 + (num6 << 15);
		}

		private static ICryptoTransform GetEncryptor(byte[] key)
		{
			Rijndael rijndael = Rijndael.Create();
			rijndael.BlockSize = 128;
			rijndael.KeySize = 256;
			rijndael.Mode = CipherMode.ECB;
			rijndael.Key = key;
			rijndael.IV = new byte[16];
			rijndael.Padding = PaddingMode.None;
			return rijndael.CreateEncryptor();
		}

		private static ICryptoTransform GetDecryptor(byte[] key)
		{
			Rijndael rijndael = Rijndael.Create();
			rijndael.BlockSize = 128;
			rijndael.KeySize = 256;
			rijndael.Mode = CipherMode.ECB;
			rijndael.Key = key;
			rijndael.IV = new byte[16];
			rijndael.Padding = PaddingMode.None;
			return rijndael.CreateDecryptor();
		}
	}
}
