﻿/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
 * ConverterSco by Cathering
 * modified by jumper
 * 
 * usage:
 * D:\ConverterSco.exe the_script_you_want_to_change.sco
 * 
 * Multiple files are not supported at this time
 * -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

using System;
using System.IO;

namespace ConverterSco
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                string inputPath = args[0];
                string inputDir = Path.GetDirectoryName(inputPath);
                string inputName = Path.GetFileNameWithoutExtension(inputPath);

                using (BinaryReader binaryReader = new BinaryReader(File.Open(inputPath, FileMode.Open)))
                {
                    int bytesToRead = 0;
                    MagicID magicId = (MagicID)binaryReader.ReadInt32();
                    int length = binaryReader.ReadInt32();
                    int num1 = binaryReader.ReadInt32();
                    int num2 = binaryReader.ReadInt32();
                    int num3 = binaryReader.ReadInt32();
                    int num4 = binaryReader.ReadInt32();
                    if (magicId == MagicID.EncryptedCompressed)
                        bytesToRead = binaryReader.ReadInt32();
                    byte[] numArray1 = (byte[])null;
                    switch (magicId)
                    {
                        case MagicID.Standard:
                            Console.WriteLine(inputPath + " is unencrypted, attempting to encrypt and compress..");
                            if (!Directory.Exists(inputDir + "\\compressed"))
                                Directory.CreateDirectory(inputDir + "\\compressed");
                            byte[] numArray2 = StreamUtil.ReadExactly(binaryReader.BaseStream, length + num1 * 4 + num2 * 4);
                            string stdPath = inputDir + "\\compressed\\" + inputName + ".sco";
                            using (FileStream fileStream = File.Create(stdPath))
                            {
                                using (BinaryWriter binaryWriter = new BinaryWriter((Stream)fileStream))
                                {
                                    binaryWriter.Write(242377555);
                                    binaryWriter.Write(length);
                                    binaryWriter.Write(num1);
                                    binaryWriter.Write(num2);
                                    binaryWriter.Write(num3);
                                    binaryWriter.Write(num4);
                                    using (MemoryStream memoryStream1 = new MemoryStream(numArray2))
                                    {
                                        memoryStream1.Position = 0L;
                                        using (MemoryStream memoryStream2 = new MemoryStream())
                                        {
                                            Compression.Compress((Stream)memoryStream1, (Stream)memoryStream2);
                                            numArray2 = memoryStream2.ToArray();
                                        }
                                    }
                                    binaryWriter.Write(numArray2.Length);
                                    byte[] buffer = Encryption.Encrypt(numArray2);
                                    binaryWriter.Write(buffer);
                                    break;
                                }
                            }
                        case MagicID.EncryptedCompressed:
                            Console.WriteLine(inputPath + " is compressed, attempting to decompress..");
                            if (!Directory.Exists(inputDir + "\\decompressed"))
                                Directory.CreateDirectory(inputDir + "\\decompressed");
                            byte[] buffer1 = Encryption.Decrypt(StreamUtil.ReadExactly(binaryReader.BaseStream, bytesToRead));
                            string ecPath = inputDir + "\\decompressed\\" + inputName + ".sco";
                            using (MemoryStream memoryStream1 = new MemoryStream(buffer1))
                            {
                                memoryStream1.Position = 0L;
                                using (MemoryStream memoryStream2 = new MemoryStream())
                                {
                                    Compression.Decompress((Stream)memoryStream1, (Stream)memoryStream2);
                                    buffer1 = memoryStream2.ToArray();
                                }
                            }
                            using (FileStream fileStream = File.Create(ecPath))
                            {
                                using (BinaryWriter binaryWriter = new BinaryWriter((Stream)fileStream))
                                {
                                    binaryWriter.Write(242377587);
                                    binaryWriter.Write(length);
                                    binaryWriter.Write(num1);
                                    binaryWriter.Write(num2);
                                    binaryWriter.Write(num3);
                                    binaryWriter.Write(num4);
                                    byte[] numArray3 = new byte[length];
                                    byte[] numArray4 = new byte[num1 * 4];
                                    byte[] numArray5 = new byte[num2 * 4];
                                    Array.Copy((Array)buffer1, (Array)numArray3, length);
                                    Array.Copy((Array)buffer1, length - 1, (Array)numArray4, 0, num1 * 4);
                                    Array.Copy((Array)buffer1, length + num1 * 4 - 1, (Array)numArray5, 0, num2 * 4);
                                    if (numArray3.Length > 1)
                                        numArray3 = Encryption.Encrypt(numArray3);
                                    if (numArray4.Length > 1)
                                        numArray4 = Encryption.Encrypt(numArray4);
                                    if (numArray5.Length > 1)
                                        numArray5 = Encryption.Encrypt(numArray5);
                                    binaryWriter.Write(numArray3);
                                    binaryWriter.Write(numArray4);
                                    binaryWriter.Write(numArray5);
                                    break;
                                }
                            }
                        case MagicID.Encrypted:
                            Console.WriteLine(inputPath + " is encrypted and not compressed, attempting to compress..");
                            if (!Directory.Exists(inputDir + "\\compressed"))
                                Directory.CreateDirectory(inputDir + "\\compressed");
                            string ePath = inputDir + "\\compressed\\" + inputName + ".sco";
                            binaryReader.BaseStream.Position = 24L;
                            byte[] buffer2 = (byte[])null;
                            byte[] buffer3 = (byte[])null;
                            byte[] buffer4 = (byte[])null;
                            if (length > 0)
                                buffer2 = Encryption.Decrypt(StreamUtil.ReadExactly(binaryReader.BaseStream, length));
                            if (num1 > 0)
                                buffer3 = Encryption.Decrypt(StreamUtil.ReadExactly(binaryReader.BaseStream, num1 * 4));
                            if (num2 > 0)
                                buffer4 = Encryption.Decrypt(StreamUtil.ReadExactly(binaryReader.BaseStream, num2 * 4));
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                memoryStream.Write(buffer2, 0, buffer2.Length);
                                if (buffer3 != null)
                                    memoryStream.Write(buffer3, 0, buffer3.Length);
                                if (buffer4 != null)
                                    memoryStream.Write(buffer4, 0, buffer4.Length);
                                numArray1 = memoryStream.ToArray();
                            }
                            using (FileStream fileStream = File.Create(ePath))
                            {
                                using (BinaryWriter binaryWriter = new BinaryWriter((Stream)fileStream))
                                {
                                    binaryWriter.Write(242377555);
                                    binaryWriter.Write(length);
                                    binaryWriter.Write(num1);
                                    binaryWriter.Write(num2);
                                    binaryWriter.Write(num3);
                                    binaryWriter.Write(num4);
                                    using (MemoryStream memoryStream1 = new MemoryStream(numArray1))
                                    {
                                        memoryStream1.Position = 0L;
                                        using (MemoryStream memoryStream2 = new MemoryStream())
                                        {
                                            Compression.Compress((Stream)memoryStream1, (Stream)memoryStream2);
                                            numArray1 = memoryStream2.ToArray();
                                        }
                                    }
                                    binaryWriter.Write(numArray1.Length);
                                    byte[] buffer5 = Encryption.Encrypt(numArray1);
                                    binaryWriter.Write(buffer5);
                                    break;
                                }
                            }
                    }
                }
            }
            else
            {
                Console.WriteLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
                Console.WriteLine("ConverterSco by Cathering");
                Console.WriteLine("modified by jumper (v{0})", typeof(Program).Assembly.GetName().Version);
                Console.WriteLine("");
                Console.WriteLine("usage:");
                Console.WriteLine("D:\\ConverterSco.exe the_script_you_want_to_change.sco");
                Console.WriteLine("");
                Console.WriteLine("Multiple files are not supported at this time");
                Console.WriteLine("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
            }
        }
    }
}
