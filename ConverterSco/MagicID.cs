﻿/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
 * ConverterSco by Cathering
 * modified by jumper
 * 
 * usage:
 * D:\ConverterSco.exe the_script_you_want_to_change.sco
 * 
 * Multiple files are not supported at this time
 * -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

namespace ConverterSco
{
	internal enum MagicID
	{
		Standard = 223494995,
		EncryptedCompressed = 242377555,
		Encrypted = 242377587,
	}
}
