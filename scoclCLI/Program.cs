﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using scoclGUI;

namespace scoclCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("scoclCLI");
            }
            else
            {
                string ns = "scoclGUI";
                INI.IniFile loadSettings = new INI.IniFile(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + ns + ".ini");
                bool ConsoleE0 = loadSettings.IniReadBool(ns, "ConsoleE0"),
                    ConsoleE1 = loadSettings.IniReadBool(ns, "ConsoleE1"),
                    ConsoleE2 = loadSettings.IniReadBool(ns, "ConsoleE2"),
                    PC1040E0 = loadSettings.IniReadBool(ns, "PC1040E0"),
                    PC1070E0 = loadSettings.IniReadBool(ns, "PC1070E0"),
                    PC1070E1 = loadSettings.IniReadBool(ns, "PC1070E1"),
                    PC1070E2 = loadSettings.IniReadBool(ns, "PC1070E2"),
                    useGlobalSeg = loadSettings.IniReadBool(ns, "useGlobalSeg"),
                    useKeepTemp = loadSettings.IniReadBool(ns, "useKeepTemp"),
                    useDebugOutput = loadSettings.IniReadBool(ns, "useDebugOutput"),
                    scanForPragma = loadSettings.IniReadBool(ns, "scanForPragma"),
                    compressAll = loadSettings.IniReadBool(ns, "compressAll");
                string OutDir = loadSettings.IniReadString(ns, "OutDir"),
                    ScoclDir = loadSettings.IniReadString(ns, "ScoclDir");
                if (!ConsoleE0 && !ConsoleE1 && !ConsoleE2 && !PC1040E0 && !PC1070E0 && !PC1070E1 && !PC1070E2)
                {
                    Console.WriteLine("scoclGUI.ini is either missing or configured wrong.");
                    Environment.Exit(1);
                }
                else if(!doesScoclExist(ScoclDir))
                {
                    Console.WriteLine("Scocl binaries not found.");
                    Environment.Exit(1);
                }
                else if (!Directory.Exists(OutDir))
                {
                    Console.WriteLine("sco output base directory does not exist.");
                    Environment.Exit(1);
                }
                else
                {
                    List<CompileSettings> compileList = new List<CompileSettings>();

                    foreach (string srcFile in args)
                    {
                        if (Path.GetExtension(srcFile) == ".c" || Path.GetExtension(srcFile) == ".cpp")
                        {
                            if (scanForPragma)
                            {
                                using (StreamReader reader = new StreamReader(srcFile))
                                {
                                    string line;
                                    while ((line = reader.ReadLine()) != null)
                                    {
                                        if (line == "#pragma once")
                                            break;
                                    }
                                }
                            }
                            if (ConsoleE0)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.PPCE0.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.PPCE0.ScoclExe,
                                    SCO.Platform.PPCE0.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (ConsoleE1)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.PPCE1.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.PPCE1.ScoclExe,
                                    SCO.Platform.PPCE1.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (ConsoleE2)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.PPCE2.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.PPCE2.ScoclExe,
                                    SCO.Platform.PPCE2.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (PC1040E0)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.X86E0P4.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.X86E0P4.ScoclExe,
                                    SCO.Platform.X86E0P4.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (PC1070E0)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.X86E0P7.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.X86E0P7.ScoclExe,
                                    SCO.Platform.X86E0P7.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (PC1070E1)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.X86E1P7.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.X86E1P7.ScoclExe,
                                    SCO.Platform.X86E1P7.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                            if (PC1070E2)
                            {
                                addToCompileList(compileList,
                                    OutDir + "\\" + SCO.Platform.X86E2P7.FriendlyName,
                                    ScoclDir + "\\" + SCO.Platform.X86E2P7.ScoclExe,
                                    SCO.Platform.X86E2P7.SigString,
                                    srcFile,
                                    useGlobalSeg,
                                    useKeepTemp,
                                    useDebugOutput,
                                    compressAll);
                            }
                        }
                    }

                    doCompile(compileList);
                }
            }
        }

        private static bool doesScoclExist(string scoclDir)
        {
            // checks for the scocl binaries in the directory listed in the textbox
            if (!File.Exists(scoclDir + "\\" + SCO.ScoclExes.oldNatives))
                return false;
            else if (!File.Exists(scoclDir + "\\" + SCO.ScoclExes.newNatives))
                return false;
            else
                return true;
        }

        private static void addToCompileList(List<CompileSettings> cList, string OutDir, string ScoclPath, string scoSig, string srcFile, bool useGlobalSeg, bool useKeepTemp, bool useDebugOutput, bool compress)
        {
            // Creates a CompileSettings object and adds it to the compile list that will be handled by the backgroundworker
            CompileSettings settings = new CompileSettings();
            settings.OutputPath = OutDir;
            settings.ScoclPath = ScoclPath;
            settings.ScoSignature = scoSig;
            settings.SourceFile = srcFile;
            settings.useGlobalSeg = useGlobalSeg;
            settings.useKeepTemp = useKeepTemp;
            settings.useDebugOutput = useDebugOutput;
            settings.Compress = compress;
            cList.Add(settings);
        }

        private static void doCompile(List<CompileSettings> cList)
        {
            // vars
            int compileTotal = cList.Count,
                successTotal = 0,
                failureTotal = 0;

            // report total number of files to be compiled
            Console.WriteLine("compiling " + compileTotal.ToString() + " files ...");
            Console.WriteLine("");

            // iterate through the list
            foreach (CompileSettings settings in cList)
            {
                // report the file and version being compiled
                Console.WriteLine("Compiling " + Path.GetFileName(settings.SourceFile) + " for " + Path.GetFileName(settings.OutputPath));

                // set up the process details to run Scocl, starting with the path to the correct binary
                ProcessStartInfo scocl = new ProcessStartInfo(settings.ScoclPath);

                // set the working directory
                scocl.WorkingDirectory = Path.GetDirectoryName(settings.ScoclPath);

                // don't use shell execute
                scocl.UseShellExecute = false;

                // set up the arguments - broken into multiple lines for readability
                scocl.Arguments = settings.ScoSignature;
                scocl.Arguments += " \"";
                scocl.Arguments += settings.SourceFile;
                scocl.Arguments += "\" \"";
                scocl.Arguments += settings.OutputPath;
                scocl.Arguments += "\\";
                scocl.Arguments += settings.GameVersion;
                scocl.Arguments += "/\"";
                if (settings.useGlobalSeg)
                    scocl.Arguments += " GLOBALSEG";
                if (settings.useKeepTemp)
                    scocl.Arguments += " KEEPTEMP";
                if (settings.useDebugOutput)
                    scocl.Arguments += " DEBUGOUTPUT";

                // redirect IO to the form
                scocl.RedirectStandardInput = true;
                scocl.RedirectStandardOutput = true;

                // don't create a new window
                scocl.CreateNoWindow = true;
                bool scoCompiled = false;

                try
                {
                    // run Scocl
                    using (Process process = Process.Start(scocl))
                    {
                        // redirect the output to a streamreader so we can send it back to the form
                        using (StreamReader reader = process.StandardOutput)
                        {
                            string result;
                            while ((result = reader.ReadLine()) != null)
                            {
                                Console.WriteLine(result);
                            }
                        }

                        // check the exit code to see if the compile was successful
                        if (process.ExitCode == 0)
                        {
                            successTotal++;
                            scoCompiled = true;

                        }
                        else
                        {
                            Console.WriteLine("Error detected! Scocl exited with code " + process.ExitCode.ToString());
                            Environment.ExitCode = 1;
                            failureTotal++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // just in case Scocl crashes (highly unlikely) there is this
                    Console.WriteLine("Scocl has crashed D:, exception info:" + ex.ToString());
                    Environment.ExitCode = 1;
                    failureTotal++;
                }
                if (scoCompiled && settings.Compress)
                {
                    string outputSco = settings.OutputPath + "\\" + Path.GetFileNameWithoutExtension(settings.SourceFile) + ".sco";

                    if (File.Exists(outputSco))
                    {
                        Console.WriteLine("Compressing...");

                        ProcessStartInfo ConverterSco = new ProcessStartInfo(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\compressor\\ConverterSco.exe");
                        ConverterSco.UseShellExecute = false;

                        // set up the arguments - broken into multiple lines for readability
                        ConverterSco.Arguments = "\"" + outputSco + "\"";

                        // redirect IO to the form
                        ConverterSco.RedirectStandardInput = true;
                        ConverterSco.RedirectStandardOutput = true;

                        // don't create a new window
                        ConverterSco.CreateNoWindow = true;

                        try
                        {
                            using (Process compressor = Process.Start(ConverterSco))
                            {
                                using (StreamReader reader = compressor.StandardOutput)
                                {
                                    string result;
                                    while ((result = reader.ReadLine()) != null)
                                    {
                                        Console.WriteLine(result);
                                    }
                                }

                                if (compressor.ExitCode == 0)
                                {
                                    Console.WriteLine("Done.  ConverterSco exited with code 0 (no errors)");
                                }
                                else
                                {
                                    Console.WriteLine("Done.  ConverterSco exited with code " + compressor.ExitCode + " (error)");
                                    Environment.ExitCode = 1;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("ConverterSco crashed.  Exception thrown:" + ex.ToString());
                            Environment.ExitCode = 1;
                        }
                    }
                }
                Console.WriteLine("");
            }
        }
    }
}
