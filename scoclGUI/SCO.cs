﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCO
{
    public static class Platform
    {
        public static class PPCE0
        {
            public static string FriendlyName = FriendlyNames.PPCE0;
            public static string ScoclExe = ScoclExes.oldNatives;
            public static string SigString = SignatureStrings.PPCE0;
            public static byte[] SigBytes = SignatureBytes.PPCE0;
        }

        public static class PPCE1
        {
            public static string FriendlyName = FriendlyNames.PPCE1;
            public static string ScoclExe = ScoclExes.oldNatives;
            public static string SigString = SignatureStrings.PPCE1;
            public static byte[] SigBytes = SignatureBytes.PPCE1;
        }

        public static class PPCE2
        {
            public static string FriendlyName = FriendlyNames.PPCE2;
            public static string ScoclExe = ScoclExes.oldNatives;
            public static string SigString = SignatureStrings.PPCE2;
            public static byte[] SigBytes = SignatureBytes.PPCE2;
        }

        public static class X86E0P4
        {
            public static string FriendlyName = FriendlyNames.X86E0P4;
            public static string ScoclExe = ScoclExes.oldNatives;
            public static string SigString = SignatureStrings.X86E0;
            public static byte[] SigBytes = SignatureBytes.X86E0;
        }

        public static class X86E0P7
        {
            public static string FriendlyName = FriendlyNames.X86E0P7;
            public static string ScoclExe = ScoclExes.newNatives;
            public static string SigString = SignatureStrings.X86E0;
            public static byte[] SigBytes = SignatureBytes.X86E0;
        }

        public static class X86E1P7
        {
            public static string FriendlyName = FriendlyNames.X86E1P7;
            public static string ScoclExe = ScoclExes.newNatives;
            public static string SigString = SignatureStrings.X86E1;
            public static byte[] SigBytes = SignatureBytes.X86E1;
        }

        public static class X86E2P7
        {
            public static string FriendlyName = FriendlyNames.X86E2P7;
            public static string ScoclExe = ScoclExes.newNatives;
            public static string SigString = SignatureStrings.X86E2;
            public static byte[] SigBytes = SignatureBytes.X86E2;
        }
    }

    public static class ScoclExes
    {
        public static string oldNatives = "scocl_old.exe";
        public static string newNatives = "scocl_new.exe";
    }

    public static class FriendlyNames
    {
        public static string PPCE0 = "IV PS3 + X360";
        public static string PPCE1 = "TLaD PS3 + X360";
        public static string PPCE2 = "TBoGT PS3 + X360";
        public static string X86E0 = "IV PC";
        public static string X86E1 = "TLaD PC";
        public static string X86E2 = "TBoGT PC";
        public static string BLANK = "Blank";
        public static string X86E0P4 = "IV PC (1.0.4.0)";
        public static string X86E0P7 = "IV PC (1.0.7.0)";
        public static string X86E1P7 = "TLaD PC (1.0.7.0)";
        public static string X86E2P7 = "TBoGT PC (1.0.7.0)";
    }
    public static class SignatureStrings
    {
        public static string PPCE0 = "0x7DD1E61C";
        public static string PPCE1 = "0x2DCD2195";
        public static string PPCE2 = "0x38E77542";
        public static string X86E0 = "0x7E204400";
        public static string X86E1 = "0x2DFAA53F";
        public static string X86E2 = "0x394BA44A";
        public static string BLANK = "0x00000000";
    }
    public static class SignatureBytes
    {
        public static byte[] PPCE0 = { 0x7D, 0xD1, 0xE6, 0x1C };
        public static byte[] PPCE1 = { 0x2D, 0xCD, 0x21, 0x95 };
        public static byte[] PPCE2 = { 0x38, 0xE7, 0x75, 0x42 };
        public static byte[] X86E0 = { 0x7E, 0x20, 0x44, 0x00 };
        public static byte[] X86E1 = { 0x2D, 0xFA, 0xA5, 0x3F };
        public static byte[] X86E2 = { 0x39, 0x4B, 0xA4, 0x4A };
        public static byte[] BLANK = { 0x00, 0x00, 0x00, 0x00 };
    }
}
