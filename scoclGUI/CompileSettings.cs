﻿using System;
using System.Collections.Generic;
using System.Text;

namespace scoclGUI
{
    public class CompileSettings
    {
        public string GameVersion { get; set; }
        public string ScoclPath { get; set; }
        public string OutputPath { get; set; }
        public string ScoSignature { get; set; }
        public string SourceFile { get; set; }
        public bool useGlobalSeg { get; set; }
        public bool useKeepTemp { get; set; }
        public bool useDebugOutput { get; set; }
        public bool Compress { get; set; }
    }
}
