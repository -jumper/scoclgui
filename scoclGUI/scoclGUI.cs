﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace scoclGUI
{
    public partial class scoclGUI : Form
    {
        #region vars
        bool formLoaded = false;
        List<string> inputList = new List<string>();
        string nl = Environment.NewLine,
            ns = typeof(Program).Namespace,
            inipath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + typeof(Program).Namespace + ".ini",
            compressorPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\compressor\\ConverterSco.exe",
            line = "────────────────────────────────────────────────────────────────────────────────";
        #endregion

        #region main form stuff
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public scoclGUI()
        {
            InitializeComponent();

            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(scoclGUI_DragEnter);
            this.DragDrop += new DragEventHandler(scoclGUI_DragDrop);
        }

        void scoclGUI_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void scoclGUI_DragDrop(object sender, DragEventArgs e)
        {
            // parse files dragged to the form
            foreach (var s in (string[])e.Data.GetData(DataFormats.FileDrop, false))
            {
                // check if the string is a directory
                if (Directory.Exists(s))
                {
                    // if so, parse files in the directory (not recursive)
                    foreach (string file in Directory.GetFiles(s))
                    {
                        // addToSourceList does further processing to determine if the file should be added
                        addToSourceList(inputList, file);
                    }
                }
                else
                {
                    // addToSourceList does further processing to determine if the file should be added
                    addToSourceList(inputList, s);
                }
            }

            // update the tool strip and compile button status
            updateToolStripAndCompileButton();

            // bring form to the front
            SetForegroundWindow(this.Handle);
        }

        private void scoclGUI_Load(object sender, EventArgs e)
        {
            // load settings from the .ini file
            loadINI(inipath);

            // not sure if this is still needed?
            formLoaded = true;

            // if directories are not set, use the included scocl files
            if (tboxOutDir.Text == "")
                tboxOutDir.Text = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\scocl\\workspace";
            if (tboxScoclDir.Text == "")
                tboxScoclDir.Text = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\scocl\\bin";

            // check for arguments (first argument will be the exe file name)
            if (Environment.GetCommandLineArgs().Length > 1)
            {
                // parse files loaded as arguments
                for (int i = 1; i < Environment.GetCommandLineArgs().Length; i++)
                {
                    // check if the string is a directory
                    if (Directory.Exists(Environment.GetCommandLineArgs()[i]))
                    {
                        // if so, parse files in the directory (not recursive)
                        foreach (string file in Directory.GetFiles(Environment.GetCommandLineArgs()[i]))
                        {
                            // addToSourceList does further processing to determine if the file should be added
                            addToSourceList(inputList, file);
                        }
                    }
                    else
                    {
                        // addToSourceList does further processing to determine if the file should be added
                        addToSourceList(inputList, Environment.GetCommandLineArgs()[i]);
                    }
                }

                // if the input list remains empty after parsing arguments,
                // resetInfoBox will clear the info box then display some usage notes
                if (inputList.Count < 1)
                    resetInfoBox(tboxInfo);
            }
            else
            {
                // resetInfoBox will clear the info box then display some usage notes
                resetInfoBox(tboxInfo);
            }

            // update the tool strip and compile button status
            updateToolStripAndCompileButton();
        }

        private void scoclGUI_Settings_CheckedChanged(object sender, EventArgs e)// most form items are wired to this event
        {
            if (formLoaded)// only update if the form is fully loaded
            {
                // save .ini file
                saveINI(inipath);

                // update the tool strip and compile button status
                updateToolStripAndCompileButton();
            }
        }
        #endregion

        #region INI load/save functions
        private void loadINI(string inifile)
        {
            INI.IniFile loadSettings = new INI.IniFile(inifile);
            cboxConsoleE0.Checked = loadSettings.IniReadBool(ns, "ConsoleE0");
            cboxConsoleE1.Checked = loadSettings.IniReadBool(ns, "ConsoleE1");
            cboxConsoleE2.Checked = loadSettings.IniReadBool(ns, "ConsoleE2");
            cboxPC1040E0.Checked = loadSettings.IniReadBool(ns, "PC1040E0");
            cboxPC1070E0.Checked = loadSettings.IniReadBool(ns, "PC1070E0");
            cboxPC1070E1.Checked = loadSettings.IniReadBool(ns, "PC1070E1");
            cboxPC1070E2.Checked = loadSettings.IniReadBool(ns, "PC1070E2");
            menuUseGLOBALSEG.CheckState = loadSettings.IniReadBool(ns, "useGlobalSeg") == true ? CheckState.Checked : CheckState.Unchecked;
            menuUseKEEPTEMP.CheckState = loadSettings.IniReadBool(ns, "useKeepTemp") == true ? CheckState.Checked : CheckState.Unchecked;
            menuUseDEBUGOUTPUT.CheckState = loadSettings.IniReadBool(ns, "useDebugOutput") == true ? CheckState.Checked : CheckState.Unchecked;
            menuScanForPragma.CheckState = loadSettings.IniReadBool(ns, "scanForPragma") == true ? CheckState.Checked : CheckState.Unchecked;
            menuCompressAll.CheckState = loadSettings.IniReadBool(ns, "compressAll") == true ? CheckState.Checked : CheckState.Unchecked;
            tboxOutDir.Text = loadSettings.IniReadString(ns, "OutDir");
            tboxScoclDir.Text = loadSettings.IniReadString(ns, "ScoclDir");
        }

        private void saveINI(string inifile)
        {
            INI.IniFile saveSettings = new INI.IniFile(inifile);
            saveSettings.IniWriteBool(ns, "ConsoleE0", cboxConsoleE0.Checked);
            saveSettings.IniWriteBool(ns, "ConsoleE1", cboxConsoleE1.Checked);
            saveSettings.IniWriteBool(ns, "ConsoleE2", cboxConsoleE2.Checked);
            saveSettings.IniWriteBool(ns, "PC1040E0", cboxPC1040E0.Checked);
            saveSettings.IniWriteBool(ns, "PC1070E0", cboxPC1070E0.Checked);
            saveSettings.IniWriteBool(ns, "PC1070E1", cboxPC1070E1.Checked);
            saveSettings.IniWriteBool(ns, "PC1070E2", cboxPC1070E2.Checked);
            saveSettings.IniWriteBool(ns, "useGlobalSeg", menuUseGLOBALSEG.CheckState == CheckState.Checked ? true : false);
            saveSettings.IniWriteBool(ns, "useKeepTemp", menuUseKEEPTEMP.CheckState == CheckState.Checked ? true : false);
            saveSettings.IniWriteBool(ns, "useDebugOutput", menuUseDEBUGOUTPUT.CheckState == CheckState.Checked ? true : false);
            saveSettings.IniWriteBool(ns, "scanForPragma", menuScanForPragma.CheckState == CheckState.Checked ? true : false);
            saveSettings.IniWriteBool(ns, "compressAll", menuCompressAll.CheckState == CheckState.Checked ? true : false);
            saveSettings.IniWriteString(ns, "OutDir", tboxOutDir.Text);
            saveSettings.IniWriteString(ns, "ScoclDir", tboxScoclDir.Text);
        }
        #endregion

        #region other functions
        private void resetInfoBox(TextBox tbox)
        {
            tbox.Clear();
            tbox.AppendText("Drag and drop .c or .cpp files anywhere to load them" + nl);
            tbox.AppendText("- or browse for them with the \"File\" menu" + nl);
            tbox.AppendText("- or drag them directly to this exe" + nl + nl);
        }

        private void addToSourceList(List<string> srcList, string srcFilePath)
        {
            // only continue on .c and .cpp files
            if (Path.GetExtension(srcFilePath) == ".c" || Path.GetExtension(srcFilePath) == ".cpp")
            {
                // only continue if the input list doesn't already contain what is being added (dupe check)
                if (!srcList.Contains(srcFilePath))
                {
                    // if enabled, scan the file line by line for "#pragma once"
                    if (menuScanForPragma.CheckState == CheckState.Checked)
                    {
                        using (StreamReader reader = new StreamReader(srcFilePath))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                if (line == "#pragma once")
                                    return;
                            }

                            // add the file to the input list
                            srcList.Add(srcFilePath);

                            // add the filename to the info box
                            tboxInfo.AppendText(srcFilePath + nl);
                        }
                    }
                    else
                    {
                        // add the file to the input list
                        srcList.Add(srcFilePath);

                        // add the filename to the info box
                        tboxInfo.AppendText(srcFilePath + nl);
                    }
                }
            }
        }

        private static void addToCompileList(List<CompileSettings> cList, string OutDir, string ScoclPath, string scoSig, string srcFile, bool useGlobalSeg, bool useKeepTemp, bool useDebugOutput, bool compress)
        {
            CompileSettings settings = new CompileSettings();
            settings.OutputPath = OutDir;
            settings.ScoclPath = ScoclPath;
            settings.ScoSignature = scoSig;
            settings.SourceFile = srcFile;
            settings.useGlobalSeg = useGlobalSeg;
            settings.useKeepTemp = useKeepTemp;
            settings.useDebugOutput = useDebugOutput;
            settings.Compress = compress;
            cList.Add(settings);
        }

        private void updateToolStripAndCompileButton()
        {
            if (inputList.Count == 0)// if the input list is empty, display idle and disable compile button if necessary
            {
                toolStripStatus.Text = "idle";

                if (btnCompile.Enabled == true)
                    btnCompile.Enabled = false;
            }
            else// display the number of sources loaded
            {
                if (inputList.Count == 1)
                    toolStripStatus.Text = inputList.Count.ToString() + " source file loaded";
                else
                    toolStripStatus.Text = inputList.Count.ToString() + " source files loaded";

                // enable the compile button if necessary - but only if scocl binaries are found
                if (btnCompile.Enabled == false)
                    btnCompile.Enabled = doesScoclExist();
            }

            // if scocl binaries are not found
            // - add the message to the status
            // - disable the compile button if necessary
            if (!doesScoclExist())
            {
                toolStripStatus.Text += " - Scocl binaries not found!";
                if (btnCompile.Enabled == true)
                    btnCompile.Enabled = false;
            }
        }

        private void enableFormItems(bool status)
        {
            // enable or disable certain form items that could disrupt the backgroundworker
            btnClear.Enabled = status;
            gboxConsole.Enabled = status;
            gboxPC4.Enabled = status;
            gboxPC7.Enabled = status;
            gboxOutput.Enabled = status;
            gboxScocl.Enabled = status;
            menuStrip1.Enabled = status;
        }

        private bool doesScoclExist()
        {
            // checks for the scocl binaries in the directory listed in the textbox
            if (!File.Exists(tboxScoclDir.Text + "\\" + SCO.ScoclExes.oldNatives))
                return false;
            else if (!File.Exists(tboxScoclDir.Text + "\\" + SCO.ScoclExes.newNatives))
                return false;
            else
                return true;
        }
        #endregion

        #region button clicks
        private void btnBrowseOutput_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogBrowseOutput = new FolderBrowserDialog();
            dialogBrowseOutput.SelectedPath = tboxOutDir.Text;
            dialogBrowseOutput.Description = "Select folder for sco file output";
            if (dialogBrowseOutput.ShowDialog() == DialogResult.OK)
            {
                tboxOutDir.Text = dialogBrowseOutput.SelectedPath;
            }
        }

        private void btnBrowseScocl_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialogBrowseScocl = new FolderBrowserDialog();
            dialogBrowseScocl.SelectedPath = tboxScoclDir.Text;
            dialogBrowseScocl.Description = "Select your scocl\\bin folder";
            if (dialogBrowseScocl.ShowDialog() == DialogResult.OK)
            {
                tboxScoclDir.Text = dialogBrowseScocl.SelectedPath;
                updateToolStripAndCompileButton();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            resetInfoBox(tboxInfo);
            inputList.Clear();
            updateToolStripAndCompileButton();
        }

        private void btnCompile_Click(object sender, EventArgs e)
        {
            // check if atleast one of the boxes is ticked
            if (!cboxConsoleE0.Checked && !cboxConsoleE1.Checked && !cboxConsoleE2.Checked && !cboxPC1040E0.Checked && !cboxPC1070E0.Checked && !cboxPC1070E1.Checked && !cboxPC1070E2.Checked)
            {
                MessageBox.Show("No boxes checked (nothing to compile)");
            }
            else
            {
                // check if output dir exists
                if (!Directory.Exists(tboxOutDir.Text))
                {
                    // prompt to browse for a new output folder
                    DialogResult dialogResult = MessageBox.Show("Output directory does not exist!" + Environment.NewLine + Environment.NewLine + "Browse for an output folder?", "Error!", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                        folderBrowserDialog1.SelectedPath = Environment.CurrentDirectory;
                        if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                        {
                            tboxOutDir.Text = folderBrowserDialog1.SelectedPath;
                        }
                    }
                }
                else
                {
                    // disable items that could disrupt the backgroundworker
                    enableFormItems(false);

                    bool useGlobalSeg = (menuUseGLOBALSEG.CheckState == CheckState.Checked ? true : false),
                        useKeepTemp = (menuUseKEEPTEMP.CheckState == CheckState.Checked ? true : false),
                        useDebugOutput = (menuUseDEBUGOUTPUT.CheckState == CheckState.Checked ? true : false),
                        scanForPragma = (menuScanForPragma.CheckState == CheckState.Checked ? true : false),
                        compressAll = (menuCompressAll.CheckState == CheckState.Checked ? true : false);
                    string OutDir = tboxOutDir.Text,
                        ScoclDir = tboxScoclDir.Text;

                    // create the list of CompileSettings to add everything to
                    List<CompileSettings> compileList = new List<CompileSettings>();

                    // parse the input list and add a CompileSettings for every version ticked
                    foreach (string srcFile in inputList)
                    {
                        if (cboxConsoleE0.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.PPCE0.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.PPCE0.ScoclExe,
                                SCO.Platform.PPCE0.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxConsoleE1.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.PPCE1.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.PPCE1.ScoclExe,
                                SCO.Platform.PPCE1.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxConsoleE2.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.PPCE2.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.PPCE2.ScoclExe,
                                SCO.Platform.PPCE2.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxPC1040E0.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.X86E0P4.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.X86E0P4.ScoclExe,
                                SCO.Platform.X86E0P4.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxPC1070E0.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.X86E0P7.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.X86E0P7.ScoclExe,
                                SCO.Platform.X86E0P7.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxPC1070E1.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.X86E1P7.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.X86E1P7.ScoclExe,
                                SCO.Platform.X86E1P7.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                        if (cboxPC1070E2.Checked)
                        {
                            addToCompileList(compileList,
                                OutDir + "\\" + SCO.Platform.X86E2P7.FriendlyName,
                                ScoclDir + "\\" + SCO.Platform.X86E2P7.ScoclExe,
                                SCO.Platform.X86E2P7.SigString,
                                srcFile,
                                useGlobalSeg,
                                useKeepTemp,
                                useDebugOutput,
                                compressAll);
                        }
                    }

                    // start the backgroundworker with the finished list of CompileSettings
                    bgworkerCompile.RunWorkerAsync(compileList);

                    // clear the input list
                    inputList.Clear();

                    // update tool strip
                    // - this will disable the compile button, since the input list was just cleared
                    updateToolStripAndCompileButton();
                }
            }
        }
        #endregion

        #region backgroundworker
        private void bgworkerCompile_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // update the info box with any updates sent by the backgroundworker
            tboxInfo.AppendText((string)e.UserState + nl);
        }

        private void bgworkerCompile_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // when the backgroundworker finishes, enable the items that were disabled
            enableFormItems(true);
        }

        private void bgworkerCompile_DoWork(object sender, DoWorkEventArgs e)
        {
            // set status to compiling
            toolStripStatus.Text = "compiling...";

            // create the list of CompileSettings sent as an argument by the button click
            List<CompileSettings> compileList = e.Argument as List<CompileSettings>;

            // vars
            int compileTotal = compileList.Count,
                successTotal = 0,
                failureTotal = 0;

            // report total number of files to be compiled
            bgworkerCompile.ReportProgress(0, nl + "compiling " + compileTotal.ToString() + " files");

            // iterate through the list
            foreach (CompileSettings settings in compileList)
            {
                // report the file and version being compiled
                bgworkerCompile.ReportProgress(0, line + nl + "Compiling " + Path.GetFileName(settings.SourceFile) + " for " + settings.GameVersion + nl);

                // set up the process details to run Scocl, starting with the path to the correct binary
                ProcessStartInfo scocl = new ProcessStartInfo(settings.ScoclPath);

                // set the working directory
                scocl.WorkingDirectory = Path.GetDirectoryName(settings.ScoclPath);

                // don't use shell execute
                scocl.UseShellExecute = false;

                // set up the arguments - broken into multiple lines for readability
                scocl.Arguments = settings.ScoSignature;
                scocl.Arguments += " \"";
                scocl.Arguments += settings.SourceFile;
                scocl.Arguments += "\" \"";
                scocl.Arguments += settings.OutputPath;
                scocl.Arguments += "\\";
                scocl.Arguments += settings.GameVersion;
                scocl.Arguments += "/\"";
                if (settings.useGlobalSeg)
                    scocl.Arguments += " GLOBALSEG";
                if (settings.useKeepTemp)
                    scocl.Arguments += " KEEPTEMP";
                if (settings.useDebugOutput)
                    scocl.Arguments += " DEBUGOUTPUT";

                // redirect IO to the form
                scocl.RedirectStandardInput = true;
                scocl.RedirectStandardOutput = true;

                // don't create a new window
                scocl.CreateNoWindow = true;
                bool scoCompiled = false;

                try
                {
                    // run Scocl
                    using (Process process = Process.Start(scocl))
                    {
                        // redirect the output to a streamreader so we can send it back to the form
                        using (StreamReader reader = process.StandardOutput)
                        {
                            string result;
                            while ((result = reader.ReadLine()) != null)
                            {
                                bgworkerCompile.ReportProgress(0, result);
                            }
                        }

                        // check the exit code to see if the compile was successful
                        if (process.ExitCode == 0)
                        {
                            successTotal++;
                            scoCompiled = true;
                            
                        }
                        else
                        {
                            bgworkerCompile.ReportProgress(0, nl + "Error detected! Scocl exited with code " + process.ExitCode.ToString());
                            failureTotal++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // just in case Scocl crashes (highly unlikely) there is this
                    bgworkerCompile.ReportProgress(0, "Scocl has crashed D:, exception info:" + nl + ex.ToString());
                    failureTotal++;
                }
                if (scoCompiled && settings.Compress)
                {
                    string outputSco = settings.OutputPath + "\\" + Path.GetFileNameWithoutExtension(settings.SourceFile) + ".sco";

                    if (File.Exists(outputSco))
                    {
                        bgworkerCompile.ReportProgress(0, nl + "Compressing...");

                        ProcessStartInfo ConverterSco = new ProcessStartInfo(compressorPath);
                        ConverterSco.UseShellExecute = false;

                        // set up the arguments - broken into multiple lines for readability
                        ConverterSco.Arguments = "\"" + outputSco + "\"";

                        // redirect IO to the form
                        ConverterSco.RedirectStandardInput = true;
                        ConverterSco.RedirectStandardOutput = true;

                        // don't create a new window
                        ConverterSco.CreateNoWindow = true;

                        try
                        {
                            using (Process compressor = Process.Start(ConverterSco))
                            {
                                using (StreamReader reader = compressor.StandardOutput)
                                {
                                    string result;
                                    while ((result = reader.ReadLine()) != null)
                                    {
                                        bgworkerCompile.ReportProgress(0, result);
                                    }
                                }

                                if (compressor.ExitCode == 0)
                                    bgworkerCompile.ReportProgress(0, nl + "Done.  ConverterSco exited with code 0 (no errors)");
                                else
                                    bgworkerCompile.ReportProgress(0, nl + "Done.  ConverterSco exited with code " + compressor.ExitCode + " (error)");
                            }
                        }
                        catch (Exception ex)
                        {
                            bgworkerCompile.ReportProgress(0, nl + "ConverterSco crashed.  Exception thrown:" + nl + ex.ToString());
                        }
                    }
                }
            }

            // set the status to the finished compile stats
            toolStripStatus.Text = "Compile finished. " + successTotal + " out of " + compileTotal + " compiled successfully (" + failureTotal + " failed to compile)";
        }
        #endregion

        #region toolstrip menu items
        private void menuAddSrc_Click(object sender, EventArgs e)
        {
            OpenFileDialog addSourceDialog = new OpenFileDialog();
            addSourceDialog.Multiselect = true;
            addSourceDialog.Filter = "scocl source files|*.c;*.cpp";
            if (addSourceDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string filename in addSourceDialog.FileNames)
                {
                    // addToSourceList does further processing to determine if the file should be added
                    addToSourceList(inputList, filename);

                    // update the tool strip and compile button status
                    updateToolStripAndCompileButton();
                }
            }
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripToggle_Click(object sender, EventArgs e)
        {
            // every tool strip item that uses a check mark uses this event to toggle the check mark
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            if (mi.CheckState == CheckState.Unchecked)
            {
                mi.CheckState = CheckState.Checked;
            }
            else
            {
                mi.CheckState = CheckState.Unchecked;
            }
        }

        private void menuAbout_Click(object sender, EventArgs e)
        {
            scoclGUI_about form = new scoclGUI_about();
            form.ShowDialog();
        }
        #endregion
    }

}
