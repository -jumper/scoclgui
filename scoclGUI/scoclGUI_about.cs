﻿using System;
using System.Windows.Forms;

namespace scoclGUI
{
    public partial class scoclGUI_about : Form
    {
        public scoclGUI_about()
        {
            InitializeComponent();
        }

        private void scoclGUI_about_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel link = sender as LinkLabel;
            System.Diagnostics.Process.Start(link.AccessibleDescription);
        }

        private void scoclGUI_about_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
