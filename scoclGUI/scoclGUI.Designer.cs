﻿namespace scoclGUI
{
    partial class scoclGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gboxConsole = new System.Windows.Forms.GroupBox();
            this.cboxConsoleE2 = new System.Windows.Forms.CheckBox();
            this.cboxConsoleE1 = new System.Windows.Forms.CheckBox();
            this.cboxConsoleE0 = new System.Windows.Forms.CheckBox();
            this.cboxPC1070E2 = new System.Windows.Forms.CheckBox();
            this.cboxPC1070E1 = new System.Windows.Forms.CheckBox();
            this.cboxPC1070E0 = new System.Windows.Forms.CheckBox();
            this.cboxPC1040E0 = new System.Windows.Forms.CheckBox();
            this.tboxInfo = new System.Windows.Forms.TextBox();
            this.tboxOutDir = new System.Windows.Forms.TextBox();
            this.btnCompile = new System.Windows.Forms.Button();
            this.bgworkerCompile = new System.ComponentModel.BackgroundWorker();
            this.gboxOutput = new System.Windows.Forms.GroupBox();
            this.btnBrowseOutput = new System.Windows.Forms.Button();
            this.gboxPC4 = new System.Windows.Forms.GroupBox();
            this.gboxPC7 = new System.Windows.Forms.GroupBox();
            this.gboxScocl = new System.Windows.Forms.GroupBox();
            this.btnBrowseScocl = new System.Windows.Forms.Button();
            this.tboxScoclDir = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAddSrc = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUseGLOBALSEG = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUseKEEPTEMP = new System.Windows.Forms.ToolStripMenuItem();
            this.menuUseDEBUGOUTPUT = new System.Windows.Forms.ToolStripMenuItem();
            this.menuScanForPragma = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCompressAll = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.gboxConsole.SuspendLayout();
            this.gboxOutput.SuspendLayout();
            this.gboxPC4.SuspendLayout();
            this.gboxPC7.SuspendLayout();
            this.gboxScocl.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gboxConsole
            // 
            this.gboxConsole.Controls.Add(this.cboxConsoleE2);
            this.gboxConsole.Controls.Add(this.cboxConsoleE1);
            this.gboxConsole.Controls.Add(this.cboxConsoleE0);
            this.gboxConsole.Location = new System.Drawing.Point(12, 27);
            this.gboxConsole.Name = "gboxConsole";
            this.gboxConsole.Size = new System.Drawing.Size(195, 44);
            this.gboxConsole.TabIndex = 0;
            this.gboxConsole.TabStop = false;
            this.gboxConsole.Text = "console";
            // 
            // cboxConsoleE2
            // 
            this.cboxConsoleE2.AutoSize = true;
            this.cboxConsoleE2.Location = new System.Drawing.Point(132, 19);
            this.cboxConsoleE2.Name = "cboxConsoleE2";
            this.cboxConsoleE2.Size = new System.Drawing.Size(61, 17);
            this.cboxConsoleE2.TabIndex = 3;
            this.cboxConsoleE2.Text = "TBoGT";
            this.cboxConsoleE2.UseVisualStyleBackColor = true;
            this.cboxConsoleE2.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxConsoleE1
            // 
            this.cboxConsoleE1.AutoSize = true;
            this.cboxConsoleE1.Location = new System.Drawing.Point(69, 19);
            this.cboxConsoleE1.Name = "cboxConsoleE1";
            this.cboxConsoleE1.Size = new System.Drawing.Size(53, 17);
            this.cboxConsoleE1.TabIndex = 2;
            this.cboxConsoleE1.Text = "TLaD";
            this.cboxConsoleE1.UseVisualStyleBackColor = true;
            this.cboxConsoleE1.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxConsoleE0
            // 
            this.cboxConsoleE0.AutoSize = true;
            this.cboxConsoleE0.Location = new System.Drawing.Point(6, 19);
            this.cboxConsoleE0.Name = "cboxConsoleE0";
            this.cboxConsoleE0.Size = new System.Drawing.Size(36, 17);
            this.cboxConsoleE0.TabIndex = 1;
            this.cboxConsoleE0.Text = "IV";
            this.cboxConsoleE0.UseVisualStyleBackColor = true;
            this.cboxConsoleE0.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxPC1070E2
            // 
            this.cboxPC1070E2.AutoSize = true;
            this.cboxPC1070E2.Location = new System.Drawing.Point(132, 19);
            this.cboxPC1070E2.Name = "cboxPC1070E2";
            this.cboxPC1070E2.Size = new System.Drawing.Size(61, 17);
            this.cboxPC1070E2.TabIndex = 14;
            this.cboxPC1070E2.Text = "TBoGT";
            this.cboxPC1070E2.UseVisualStyleBackColor = true;
            this.cboxPC1070E2.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxPC1070E1
            // 
            this.cboxPC1070E1.AutoSize = true;
            this.cboxPC1070E1.Location = new System.Drawing.Point(69, 19);
            this.cboxPC1070E1.Name = "cboxPC1070E1";
            this.cboxPC1070E1.Size = new System.Drawing.Size(53, 17);
            this.cboxPC1070E1.TabIndex = 13;
            this.cboxPC1070E1.Text = "TLaD";
            this.cboxPC1070E1.UseVisualStyleBackColor = true;
            this.cboxPC1070E1.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxPC1070E0
            // 
            this.cboxPC1070E0.AutoSize = true;
            this.cboxPC1070E0.Location = new System.Drawing.Point(6, 19);
            this.cboxPC1070E0.Name = "cboxPC1070E0";
            this.cboxPC1070E0.Size = new System.Drawing.Size(36, 17);
            this.cboxPC1070E0.TabIndex = 12;
            this.cboxPC1070E0.Text = "IV";
            this.cboxPC1070E0.UseVisualStyleBackColor = true;
            this.cboxPC1070E0.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // cboxPC1040E0
            // 
            this.cboxPC1040E0.AutoSize = true;
            this.cboxPC1040E0.Location = new System.Drawing.Point(6, 18);
            this.cboxPC1040E0.Name = "cboxPC1040E0";
            this.cboxPC1040E0.Size = new System.Drawing.Size(36, 17);
            this.cboxPC1040E0.TabIndex = 7;
            this.cboxPC1040E0.Text = "IV";
            this.cboxPC1040E0.UseVisualStyleBackColor = true;
            this.cboxPC1040E0.CheckedChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // tboxInfo
            // 
            this.tboxInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tboxInfo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.tboxInfo.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxInfo.Location = new System.Drawing.Point(12, 177);
            this.tboxInfo.Multiline = true;
            this.tboxInfo.Name = "tboxInfo";
            this.tboxInfo.ReadOnly = true;
            this.tboxInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxInfo.Size = new System.Drawing.Size(584, 229);
            this.tboxInfo.TabIndex = 0;
            // 
            // tboxOutDir
            // 
            this.tboxOutDir.Location = new System.Drawing.Point(6, 17);
            this.tboxOutDir.Name = "tboxOutDir";
            this.tboxOutDir.Size = new System.Drawing.Size(342, 20);
            this.tboxOutDir.TabIndex = 2;
            this.tboxOutDir.TextChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // btnCompile
            // 
            this.btnCompile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCompile.Enabled = false;
            this.btnCompile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCompile.Location = new System.Drawing.Point(378, 127);
            this.btnCompile.Name = "btnCompile";
            this.btnCompile.Size = new System.Drawing.Size(218, 44);
            this.btnCompile.TabIndex = 3;
            this.btnCompile.Text = "start compile";
            this.btnCompile.Click += new System.EventHandler(this.btnCompile_Click);
            // 
            // bgworkerCompile
            // 
            this.bgworkerCompile.WorkerReportsProgress = true;
            this.bgworkerCompile.WorkerSupportsCancellation = true;
            this.bgworkerCompile.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgworkerCompile_DoWork);
            this.bgworkerCompile.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgworkerCompile_ProgressChanged);
            this.bgworkerCompile.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgworkerCompile_RunWorkerCompleted);
            // 
            // gboxOutput
            // 
            this.gboxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gboxOutput.Controls.Add(this.btnBrowseOutput);
            this.gboxOutput.Controls.Add(this.tboxOutDir);
            this.gboxOutput.Location = new System.Drawing.Point(212, 27);
            this.gboxOutput.Name = "gboxOutput";
            this.gboxOutput.Size = new System.Drawing.Size(384, 44);
            this.gboxOutput.TabIndex = 4;
            this.gboxOutput.TabStop = false;
            this.gboxOutput.Text = "sco output";
            // 
            // btnBrowseOutput
            // 
            this.btnBrowseOutput.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnBrowseOutput.Location = new System.Drawing.Point(354, 17);
            this.btnBrowseOutput.Name = "btnBrowseOutput";
            this.btnBrowseOutput.Size = new System.Drawing.Size(24, 19);
            this.btnBrowseOutput.TabIndex = 7;
            this.btnBrowseOutput.Text = "...";
            this.btnBrowseOutput.UseVisualStyleBackColor = false;
            this.btnBrowseOutput.Click += new System.EventHandler(this.btnBrowseOutput_Click);
            // 
            // gboxPC4
            // 
            this.gboxPC4.Controls.Add(this.cboxPC1040E0);
            this.gboxPC4.Location = new System.Drawing.Point(12, 77);
            this.gboxPC4.Name = "gboxPC4";
            this.gboxPC4.Size = new System.Drawing.Size(195, 44);
            this.gboxPC4.TabIndex = 5;
            this.gboxPC4.TabStop = false;
            this.gboxPC4.Text = "pc (1.0.4.0)";
            // 
            // gboxPC7
            // 
            this.gboxPC7.Controls.Add(this.cboxPC1070E2);
            this.gboxPC7.Controls.Add(this.cboxPC1070E0);
            this.gboxPC7.Controls.Add(this.cboxPC1070E1);
            this.gboxPC7.Location = new System.Drawing.Point(12, 127);
            this.gboxPC7.Name = "gboxPC7";
            this.gboxPC7.Size = new System.Drawing.Size(195, 44);
            this.gboxPC7.TabIndex = 6;
            this.gboxPC7.TabStop = false;
            this.gboxPC7.Text = "pc (1.0.7.0 +)";
            // 
            // gboxScocl
            // 
            this.gboxScocl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gboxScocl.Controls.Add(this.btnBrowseScocl);
            this.gboxScocl.Controls.Add(this.tboxScoclDir);
            this.gboxScocl.Location = new System.Drawing.Point(212, 77);
            this.gboxScocl.Name = "gboxScocl";
            this.gboxScocl.Size = new System.Drawing.Size(384, 44);
            this.gboxScocl.TabIndex = 7;
            this.gboxScocl.TabStop = false;
            this.gboxScocl.Text = "scocl location";
            // 
            // btnBrowseScocl
            // 
            this.btnBrowseScocl.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnBrowseScocl.Location = new System.Drawing.Point(354, 16);
            this.btnBrowseScocl.Name = "btnBrowseScocl";
            this.btnBrowseScocl.Size = new System.Drawing.Size(24, 19);
            this.btnBrowseScocl.TabIndex = 7;
            this.btnBrowseScocl.Text = "...";
            this.btnBrowseScocl.UseVisualStyleBackColor = false;
            this.btnBrowseScocl.Click += new System.EventHandler(this.btnBrowseScocl_Click);
            // 
            // tboxScoclDir
            // 
            this.tboxScoclDir.Location = new System.Drawing.Point(6, 16);
            this.tboxScoclDir.Name = "tboxScoclDir";
            this.tboxScoclDir.Size = new System.Drawing.Size(342, 20);
            this.tboxScoclDir.TabIndex = 2;
            this.tboxScoclDir.TextChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClear.Location = new System.Drawing.Point(213, 127);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(160, 44);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "clear input";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.menuAbout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(608, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddSrc,
            this.menuExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // menuAddSrc
            // 
            this.menuAddSrc.Name = "menuAddSrc";
            this.menuAddSrc.Size = new System.Drawing.Size(158, 22);
            this.menuAddSrc.Text = "Add source files";
            this.menuAddSrc.Click += new System.EventHandler(this.menuAddSrc_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(158, 22);
            this.menuExit.Text = "Exit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuUseGLOBALSEG,
            this.menuUseKEEPTEMP,
            this.menuUseDEBUGOUTPUT,
            this.menuScanForPragma,
            this.menuCompressAll});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // menuUseGLOBALSEG
            // 
            this.menuUseGLOBALSEG.Name = "menuUseGLOBALSEG";
            this.menuUseGLOBALSEG.Size = new System.Drawing.Size(196, 22);
            this.menuUseGLOBALSEG.Text = "use GLOBALSEG";
            this.menuUseGLOBALSEG.CheckStateChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            this.menuUseGLOBALSEG.Click += new System.EventHandler(this.toolStripToggle_Click);
            // 
            // menuUseKEEPTEMP
            // 
            this.menuUseKEEPTEMP.Name = "menuUseKEEPTEMP";
            this.menuUseKEEPTEMP.Size = new System.Drawing.Size(196, 22);
            this.menuUseKEEPTEMP.Text = "use KEEPTEMP";
            this.menuUseKEEPTEMP.CheckStateChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            this.menuUseKEEPTEMP.Click += new System.EventHandler(this.toolStripToggle_Click);
            // 
            // menuUseDEBUGOUTPUT
            // 
            this.menuUseDEBUGOUTPUT.Name = "menuUseDEBUGOUTPUT";
            this.menuUseDEBUGOUTPUT.Size = new System.Drawing.Size(196, 22);
            this.menuUseDEBUGOUTPUT.Text = "use DEBUGOUTPUT";
            this.menuUseDEBUGOUTPUT.CheckStateChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            this.menuUseDEBUGOUTPUT.Click += new System.EventHandler(this.toolStripToggle_Click);
            // 
            // menuScanForPragma
            // 
            this.menuScanForPragma.Name = "menuScanForPragma";
            this.menuScanForPragma.Size = new System.Drawing.Size(196, 22);
            this.menuScanForPragma.Text = "scan for #pragma once";
            this.menuScanForPragma.CheckStateChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            this.menuScanForPragma.Click += new System.EventHandler(this.toolStripToggle_Click);
            // 
            // menuCompressAll
            // 
            this.menuCompressAll.Name = "menuCompressAll";
            this.menuCompressAll.Size = new System.Drawing.Size(196, 22);
            this.menuCompressAll.Text = "compress all output";
            this.menuCompressAll.CheckStateChanged += new System.EventHandler(this.scoclGUI_Settings_CheckedChanged);
            this.menuCompressAll.Click += new System.EventHandler(this.toolStripToggle_Click);
            // 
            // menuAbout
            // 
            this.menuAbout.Name = "menuAbout";
            this.menuAbout.Size = new System.Drawing.Size(52, 20);
            this.menuAbout.Text = "About";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 420);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(608, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(53, 17);
            this.toolStripStatus.Text = "scoclGUI";
            // 
            // scoclGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 442);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.gboxScocl);
            this.Controls.Add(this.tboxInfo);
            this.Controls.Add(this.gboxPC7);
            this.Controls.Add(this.gboxPC4);
            this.Controls.Add(this.gboxOutput);
            this.Controls.Add(this.btnCompile);
            this.Controls.Add(this.gboxConsole);
            this.Controls.Add(this.menuStrip1);
            this.Icon = global::scoclGUI.Properties.Resources.iv_teal_new;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(624, 480);
            this.Name = "scoclGUI";
            this.Text = "scoclGUI";
            this.Load += new System.EventHandler(this.scoclGUI_Load);
            this.gboxConsole.ResumeLayout(false);
            this.gboxConsole.PerformLayout();
            this.gboxOutput.ResumeLayout(false);
            this.gboxOutput.PerformLayout();
            this.gboxPC4.ResumeLayout(false);
            this.gboxPC4.PerformLayout();
            this.gboxPC7.ResumeLayout(false);
            this.gboxPC7.PerformLayout();
            this.gboxScocl.ResumeLayout(false);
            this.gboxScocl.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboxConsole;
        private System.Windows.Forms.CheckBox cboxPC1070E2;
        private System.Windows.Forms.CheckBox cboxPC1070E1;
        private System.Windows.Forms.CheckBox cboxPC1070E0;
        private System.Windows.Forms.CheckBox cboxPC1040E0;
        private System.Windows.Forms.CheckBox cboxConsoleE2;
        private System.Windows.Forms.CheckBox cboxConsoleE1;
        private System.Windows.Forms.CheckBox cboxConsoleE0;
        private System.Windows.Forms.TextBox tboxInfo;
        private System.Windows.Forms.TextBox tboxOutDir;
        private System.Windows.Forms.Button btnCompile;
        private System.ComponentModel.BackgroundWorker bgworkerCompile;
        private System.Windows.Forms.GroupBox gboxOutput;
        private System.Windows.Forms.GroupBox gboxPC4;
        private System.Windows.Forms.GroupBox gboxPC7;
        private System.Windows.Forms.Button btnBrowseOutput;
        private System.Windows.Forms.GroupBox gboxScocl;
        private System.Windows.Forms.Button btnBrowseScocl;
        private System.Windows.Forms.TextBox tboxScoclDir;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuAddSrc;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuUseGLOBALSEG;
        private System.Windows.Forms.ToolStripMenuItem menuUseKEEPTEMP;
        private System.Windows.Forms.ToolStripMenuItem menuUseDEBUGOUTPUT;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatus;
        private System.Windows.Forms.ToolStripMenuItem menuScanForPragma;
        private System.Windows.Forms.ToolStripMenuItem menuCompressAll;
    }
}

